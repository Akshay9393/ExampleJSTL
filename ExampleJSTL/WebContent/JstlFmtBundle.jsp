<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JSTL fmt:bundle Tag</title>
</head>
<body>
<fmt:setBundle basename="com.java.org.Pop" scope="jsp_scope">
<c:set var="department_name"> value="<fmt:message key="key.name"/>" </c:set>
<c:set var="department_Address"> value="<fmt:message key="key.Address"/>" </c:set>
<c:set var="department_Number"> value="<fmt:message key="key.Number"/>" </c:set>
</fmt:bundle>

<c:out value="${department_Address}"/>

<c:out value="${department_Number}"/>

</body>
</html>